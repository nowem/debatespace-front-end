import Vue from "vue";
import moment from "moment";
import App from "./App.vue";

import router from "@/router";
import store from "@/store";

import vuetify from "@/plugins/vuetify";
import VueNotify from "@/plugins/notify";
import apolloProvider from "@/plugins/apollo";
// import filters from "@/plugins/apollo";

import "roboto-fontface/css/roboto/roboto-fontface.css";
import "@mdi/font/css/materialdesignicons.css";

import "@/css/main.scss";

Vue.use(new VueNotify());
// Vue.use(filters);
Vue.filter("formatDate", value => {
  if (value) return moment(value).format("MM.DD.YYYY");
});

Vue.config.productionTip = false;

const DEFAULT_TITLE = "Some Default Title";
router.afterEach((to, from) => {
  document.title = to.meta.title || DEFAULT_TITLE;
});

new Vue({
  router,
  store,
  vuetify,
  apolloProvider,
  beforeCreate() {
    const token = this.$cookies.get("token");

    if (token) {
      this.$store
        .dispatch("auth/recover", token)
        .catch(this.$store.dispatch("auth/reset"));
    }
  },
  render: h => h(App)
}).$mount("#app");
