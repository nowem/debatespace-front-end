import { ApolloClient } from "apollo-client";
import { createUploadLink } from "apollo-upload-client";
import { setContext } from "apollo-link-context";
import { InMemoryCache } from "apollo-cache-inmemory";

import Vue from "vue";
import VueApollo from "vue-apollo";

import cookies from "@/plugins/cookies";

const AUTH_TOKEN = "token";

const cache = new InMemoryCache();

const linkOptions = {
  uri: "https://debateit.io/api/graphql"
};

const link = createUploadLink(linkOptions);

const authLink = setContext((_, { headers }) => {
  const token = cookies.get(AUTH_TOKEN);

  return {
    headers: {
      ...headers,
      authorization: token ? `${token}` : "",
      "Access-Control-Allow-Origin": "*"
    }
  };
});

// Manually call this when user log in
export async function onLogin(apolloClient, token) {
  if (typeof localStorage !== "undefined" && token) {
    localStorage.setItem(AUTH_TOKEN, token);
  }
  // if (apolloClient.wsClient) restartWebsockets(apolloClient.wsClient);
  try {
    await apolloClient.resetStore();
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log("%cError on cache reset (login)", "color: orange;", e.message);
  }
}

// Manually call this when user log out
export async function onLogout(apolloClient) {
  if (typeof localStorage !== "undefined") {
    localStorage.removeItem(AUTH_TOKEN);
  }
  // if (apolloClient.wsClient) restartWebsockets(apolloClient.wsClient);
  try {
    await apolloClient.resetStore();
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log("%cError on cache reset (logout)", "color: orange;", e.message);
  }
}

export const client = new ApolloClient({
  link: authLink.concat(link),
  // link,
  cache
  // connectToDevTools: true
});

Vue.use(VueApollo);

export default new VueApollo({
  defaultClient: client,
  errorHandler(error) {
    // eslint-disable-next-line no-console
    console.log(
      "%cError",
      "background: red; color: white; padding: 2px 4px; border-radius: 3px; font-weight: bold;",
      error.message
    );
  }
});
