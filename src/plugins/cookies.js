import Vue from "vue";
import Cookies from "js-cookie";

const VueCookies = {
  install: function(Vue) {
    Vue.prototype.$cookies = Cookies;
  }
};

Vue.use(VueCookies);

export default Cookies;
