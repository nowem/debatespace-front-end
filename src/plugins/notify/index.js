import Vue from "vue";
import Notification from "./Notification";

class VueNotify {
  constructor(options) {
    this.component = null;
    this.queue = [];
    this.options = options;
  }

  createNotification(options) {
    this.component = new Vue(Notification, options);

    Object.assign(this.component, this.options, options);

    this.component.$on("statusChange", (isActive, wasActive) => {
      if (wasActive && !isActive) {
        this.component.$nextTick(() => {
          this.component.$destroy();
          this.component = null;

          if (this.queue.length) {
            let toast = this.queue.shift();
            this.show(toast.text, toast.options);
          }
        });
      }
    });

    document.body.appendChild(this.component.$mount().$el);
  }

  install(Vue) {
    Vue.prototype.$notify = this;
  }

  show(text, options) {
    if (!this.component) {
      this.createNotification({ text, ...options });
    } else {
      this.queue.push({ text, options });
    }
  }

  error(text, options) {
    const message = text.replace("GraphQL error: ", "").trim();

    this.show(message, options);
  }

  // shortcuts(options) {
  //   const colors = ["success", "info", "error", "warning"];
  //   const methods = {};

  //   colors.forEach(color => {
  //     methods[color] = (message, options) =>
  //       this.show(message, { color, ...options });
  //   });

  //   if (options.shorts) {
  //     for (let key in options.shorts) {
  //       let localOptions = options.shorts[key];
  //       methods[key] = (message, options) =>
  //         this.show(message, { ...localOptions, ...options });
  //     }
  //   }

  //   return methods;
  // }
}

export default VueNotify;
