import Vue from "vue";
import Vuetify from "vuetify/lib";

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: "mdi"
  },
  theme: {
    themes: {
      light: {
        primary: "#e5c780",
        secondary: "#c62703",
        accent: "#8e8d8a",

        background: "#66676b",
        foreground: "#d8cca1",
        dominant: "#d8cca1"
      }
    }
  }
});
