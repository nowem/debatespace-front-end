import gql from "graphql-tag";

import userInfoFragment from "./fragments/user-info";
import debateInfoFragment from "./fragments/debate-info";
import participantsFragment from "./fragments/participants";

const queries = {
  feed: gql`
    query feed($page: Int!) {
      feed(page: $page) {
        hasMore
        page {
          ... on Debate {
            ...DebateInfo
            ...Participants
          }
        }
      }
    }

    ${debateInfoFragment}
    ${participantsFragment}
  `,
  addArgument: gql`
    mutation addArgument($debate: ID!, $argument: String!) {
      addArgument(debate: $debate, argument: $argument) {
        side
        timestamp
        content
      }
    }
  `,

  follow: gql`
    mutation follow($username: String!) {
      follow(username: $username) {
        ...UserInfo

        stats {
          followers
          debates
          points
          category
        }
      }
    }

    ${userInfoFragment}
  `,
  updateInfo: gql`
    mutation updateInfo($name: String, $country: String, $about: String) {
      updateInfo(name: $name, country: $country, about: $about) {
        about
        name
        country
        picture
      }
    }
  `,
  uploadImage: gql`
    mutation uploadImage($file: Upload!) {
      uploadImage(file: $file) {
        status
        error
        url
      }
    }
  `,
  vote: gql`
    mutation vote($debate: ID!, $side: DebateSide!) {
      vote(debate: $debate, side: $side) {
        ...DebateInfo
        ...Participants
      }
    }

    ${debateInfoFragment}
    ${participantsFragment}
  `
};

export default queries;
