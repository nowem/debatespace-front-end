import gql from "graphql-tag";

import BaseUserAccountInfo from "@/queries/fragments/base-user-account-info";

export default {
  login: gql`
    mutation login($username: String!, $password: String!) {
      login(username: $username, password: $password) {
        status
        error
        result {
          token
          user {
            ...BaseUserAccountInfo
          }
        }
      }
    }

    ${BaseUserAccountInfo}
  `,
  recover: gql`
    query recover($token: String!) {
      authenticate(token: $token) {
        status
        error
        result {
          token
          user {
            ...BaseUserAccountInfo
          }
        }
      }
    }

    ${BaseUserAccountInfo}
  `,

  signup: gql`
    mutation signup(
      $username: String!
      $email: String!
      $password: String!
      $options: UserOptions
    ) {
      signup(
        username: $username
        email: $email
        password: $password
        options: $options
      ) {
        status
        error
        result {
          token
          user {
            ...BaseUserAccountInfo
          }
        }
      }
    }

    ${BaseUserAccountInfo}
  `
};
