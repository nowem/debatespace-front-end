import gql from "graphql-tag";
import DebateInfoFragment from "./fragments/debate-info";
import ParticipantFragment from "./fragments/participants";

export const debates = gql`
  query debates($page: Int!, $size: Int!) {
    debates(page: $page, size: $size) {
      hasMore
      page {
        ... on Debate {
          ...DebateInfo
          ...Participants
        }
      }
    }
  }

  ${DebateInfoFragment}
  ${ParticipantFragment}
`;

export const debate = gql`
  query debate($id: ID!) {
    debate(id: $id) {
      ...DebateInfo
      ...Participants

      log {
        side
        content
        timestamp
      }
    }
  }

  ${DebateInfoFragment}
  ${ParticipantFragment}
`;

export const debatesByTag = gql`
  query searchTag($tags: [String!]!) {
    searchTag(tags: $tags) {
      ...DebateInfo
      ...Participants
    }
  }

  ${DebateInfoFragment}
  ${ParticipantFragment}
`;

export const debatesByCategory = gql`
  query searchCategory($category: String!) {
    searchCategory(category: $category) {
      ...DebateInfo
      ...Participants
    }
  }

  ${DebateInfoFragment}
  ${ParticipantFragment}
`;

export const deepSearch = gql`
  query deepSearch($query: String!) {
    deepSearch(query: $query) {
      ...DebateInfo
      ...Participants
    }
  }

  ${DebateInfoFragment}
  ${ParticipantFragment}
`;

export default {
  debate,
  debates,
  debatesByCategory,
  debatesByTag,
  deepSearch
};
