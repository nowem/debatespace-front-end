import gql from "graphql-tag";

import userInfoFragment from "../fragments/user-info";
import debateInfoFragment from "../fragments/debate-info";
import participantsFragment from "../fragments/participants";
import requestInfoFragment from "../fragments/request-info";

export const userNotifications = gql`
  fragment UserNotifications on User {
    notifications {
      time
      message
    }
  }
`;

export const userStats = gql`
  fragment UserStats on User {
    stats {
      points
      debates
      followers
      category
    }
  }
`;

export const userDebates = gql`
  fragment UserDebates on User {
    debates {
      ...DebateInfo
      ...Participants
    }
  }
  ${debateInfoFragment}
  ${participantsFragment}
`;

export const userRequests = gql`
  fragment UserRequests on User {
    requests {
      incoming {
        ...RequestInfo
      }
      outgoing {
        ...RequestInfo
      }
    }
  }

  ${requestInfoFragment}
`;

export default gql`
  fragment BaseUserAccountInfo on User {
    relation

    ...UserInfo
    ...UserStats
    ...UserNotifications
    ...UserDebates
    ...UserRequests
  }

  ${userInfoFragment}
  ${userRequests}
  ${userStats}
  ${userDebates}
  ${userNotifications}
`;
