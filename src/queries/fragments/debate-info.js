import gql from "graphql-tag";

export default gql`
  fragment DebateInfo on Debate {
    id
    topic
    deadline
    created
    tags
    votedOn
    turn

    category {
      name
    }
  }
`;
