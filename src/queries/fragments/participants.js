import gql from "graphql-tag";
import userInfoFragment from "../fragments/user-info";

export default gql`
  fragment Participants on Debate {
    sides {
      left {
        score
        user {
          ...UserInfo
        }
      }
      right {
        score
        user {
          ...UserInfo
        }
      }
    }
  }

  ${userInfoFragment}
`;
