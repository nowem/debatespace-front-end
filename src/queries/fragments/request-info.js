import gql from "graphql-tag";
import userInfoFragment from "../fragments/user-info";

export default gql`
  fragment RequestInfo on DebateRequest {
    id
    time
    status

    initiator {
      ...UserInfo

      stats {
        points
      }
    }

    recipient {
      ...UserInfo

      stats {
        points
      }
    }

    debate {
      topic
      tags

      category {
        name
      }
    }
  }

  ${userInfoFragment}
`;
