import gql from "graphql-tag";

import debateInfoFragment from "../fragments/debate-info";
import participantsFragment from "../fragments/participants";

export default gql`
  fragment UserDebates on User {
    debates {
      ...DebateInfo
      ...Participants
    }
  }

  ${debateInfoFragment}
  ${participantsFragment}
`;
