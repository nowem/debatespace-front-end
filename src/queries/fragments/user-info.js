import gql from "graphql-tag";

export default gql`
  fragment UserInfo on User {
    info {
      name
      username
      email
      color
      picture
      level
      about
    }
  }
`;
