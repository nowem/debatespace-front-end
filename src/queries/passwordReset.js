import gql from "graphql-tag";

export const reset = gql`
  mutation resetPassword($identifier: String!, $password: String!) {
    resetPassword(identifier: $identifier, password: $password) {
      status
      error
    }
  }
`;

export const verify = gql`
  query verifyPasswordReset($identifier: String!) {
    verifyPasswordResetIdentifier(identifier: $identifier) {
      status
      error
    }
  }
`;

export default { reset, verify };