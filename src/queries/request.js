import gql from "graphql-tag";

import UserInfoFragment from "./fragments/user-info";
import RequestInfoFragment from "@/queries/fragments/request-info";
import DebateInfoFragment from "@/queries/fragments/debate-info";
import ParticipantFragment from "@/queries/fragments/participants";

export const createRequest = gql`
  mutation createRequest(
    $initiator: String!
    $recipient: String!
    $options: DebateInput!
  ) {
    createRequest(
      initiator: $initiator
      recipient: $recipient
      options: $options
    ) {
      ...RequestInfo
    }
  }

  ${RequestInfoFragment}
  ${UserInfoFragment}
`;
export const resolveRequest = gql`
  mutation resolveRequest($request: ID!, $status: RequestStatus!) {
    resolveRequest(request: $request, status: $status) {
      ...DebateInfo
      ...Participants
    }
  }

  ${DebateInfoFragment}
  ${ParticipantFragment}
`;

export const debateRequests = gql`
  query debateRequests {
    requests(source: INCOMING) {
      id
      time
      status

      initiator {
        ...UserInfo

        stats {
          points
        }
      }

      recipient {
        ...UserInfo
      }

      debate {
        topic
        tags
        category {
          name
        }
      }
    }
  }

  ${UserInfoFragment}
`;

export default {
  createRequest,
  resolveRequest,
  debateRequests
};
