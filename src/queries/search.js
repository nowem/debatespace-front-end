import gql from "graphql-tag";

import userInfoFragment from "@/queries/fragments/user-info";

export const searchUsers = gql`
  query searchUsers($filters: UserSearchOptions) {
    searchUsers(filters: $filters) {
      ...UserInfo

      stats {
        points
      }
    }
  }

  ${userInfoFragment}
`;

export const followers = gql`
  query followers {
    followers {
      ...UserInfo

      stats {
        points
      }
    }
  }

  ${userInfoFragment}
`;

export default {
  users: searchUsers,
  followers
};
