import gql from "graphql-tag";
import userInfo from "@/queries/fragments/user-info";
import {
  userDebates,
  userStats
} from "@/queries/fragments/base-user-account-info";

export default gql`
  query user($username: String!) {
    user(username: $username) {
      relation
      ...UserInfo
      ...UserDebates
      ...UserStats
    }
  }

  ${userInfo}
  ${userDebates}
  ${userStats}
`;
