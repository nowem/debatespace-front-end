import gql from "graphql-tag";

import userInfoFragment from "../fragments/user-info";

export default gql`
  query {
    users {
      info {
        ...UserInfo
      }

      stats {
        followers
        debates
        points
        category
      }
    }
  }

  ${userInfoFragment}
`;
