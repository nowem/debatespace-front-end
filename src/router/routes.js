export default [
  {
    path: "/",
    component: () => import("@/layouts/TemporaryLayout.vue"),
    children: [
      {
        path: "",
        name: "home",
        component: () => import("@/pages/Index.vue"),
        meta: {
          title: "Debatespace"
        }
      },
      {
        path: "forgot",
        name: "forgot",
        component: () => import("@/pages/PasswordReset.vue"),
        meta: {
          title: "Password Reset"
        }
      },
      {
        path: "dashboard",
        component: () => import("@/components/dashboard/DashboardLayout"),
        meta: {
          title: "Dashboard | Debatespace"
        },
        children: [
          {
            path: "",
            component: () =>
              import(
                /* webpackChunkName: "feed" */ "@/components/dashboard/DashboardFeed"
              )
          },
          {
            path: "feed",
            component: () =>
              import(
                /* webpackChunkName: "feed" */ "@/components/dashboard/DashboardFeed"
              )
          },
          {
            path: "debates",
            component: () =>
              import(
                /* webpackChunkName: "current-debates" */ "@/components/dashboard/DashboardDebates"
              )
          },
          {
            path: "requests",
            component: () =>
              import(
                /* webpackChunkName: "current-requests" */ "@/components/dashboard/DashboardRequests"
              )
          },
          {
            path: "settings",
            component: () =>
              import(
                /* webpackChunkName: "settings" */ "@/components/dashboard/DashboardSettings"
              )
          },
          {
            path: "profile",
            component: () =>
              import(
                /* webpackChunkName: "current-profile" */ "@/components/dashboard/DashboardProfile"
              )
          }
        ]
      },
      {
        path: "about",
        name: "about",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
          import(/* webpackChunkName: "about" */ "@/pages/About.vue")
      },
      {
        path: "download",
        name: "download",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
          import(/* webpackChunkName: "download" */ "@/pages/DownloadPage.vue")
      },
      {
        path: "debate/initiate",
        component: () =>
          import(
            /* webpackChunkName: "initiate" */ "@/pages/InitiateDebate.vue"
          ),
        meta: {
          title: "Initiate Debate | Debatespace"
        }
      },
      {
        path: "debate/:id",
        component: () =>
          import(/* webpackChunkName: "debate" */ "@/pages/Debate.vue")
      },
      {
        path: "user/:username",
        component: () =>
          import(/* webpackChunkName: "profile" */ "@/pages/Profile.vue")
      },
      {
        path: "register",
        component: () =>
          import(/* webpackChunkName: "register" */ "@/pages/Register.vue")
      },
      {
        path: "search",
        component: () => import("@/pages/Search.vue"),
        children: [
          {
            path: "tag",
            component: () =>
              import(
                /* webpackChunkName: "tagSearch" */ "@/components/search/SearchTags.vue"
              )
          },
          {
            path: "category/:categoryname",
            component: () =>
              import(
                /* webpackChunkName: "tagSearch" */ "@/components/search/SearchCategory.vue"
              )
          },
          {
            path: "debates",
            component: () => import("@/components/search/DeepSearch.vue")
          }
        ]
      }
    ]
  },
  {
    path: "*",
    component: () => import("@/pages/Error404.vue")
  }
];
