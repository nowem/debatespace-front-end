import Vue from "vue";
import Vuex from "vuex";

import auth from "./modules/auth";
import account from "./modules/account";
import debates from "./modules/debates";
import search from "./modules/search";
import passwordReset from "./modules/passwordReset";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    account,
    debates,
    search,
    passwordReset
  },
  strict: process.env.DEV
});
