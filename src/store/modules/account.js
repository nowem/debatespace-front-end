import { client } from "@/plugins/apollo";

import queries from "@/queries/actions";

const state = () => ({
  user: {}
});

const getters = {
  user: state => state.user
};

const mutations = {
  load_user(state, { user }) {
    state.user = Object.assign({}, state, user);
  },

  remove_user(state) {
    state.user = {};
  },

  add_request(state, { request, status }) {
    state.user.debates.requests[status].push(request);
  },
  add_image(state, { image }) {
    state.user.info.picture = image;
  },

  add_new_debate(state, debate) {
    if (state.user.debates) {
      state.user.debates.ongoing.unshift(debate);
      return debate;
    }
  },

  update_info(state, info) {
    state.user.info = Object.assign({}, state.user.info, info);
  },

  remove_request(state, request) {
    const id = request.id || request;
    if (state.user.requests && state.user.requests.incoming) {
      const idx = state.user.requests.incoming.findIndex(r => r.id == id);
      state.user.requests.incoming.splice(idx, 1);
    }
  }
};

const actions = {
  loadUser({ commit }, user) {
    return Promise.resolve().then(() => {
      commit("load_user", { user });
    });
  },
  logout({ commit }) {
    return Promise.resolve().then(() => {
      commit("remove_user");
    });
  },
  vote(_, voteOptions) {
    return client
      .mutate({
        mutation: queries.vote,
        variables: voteOptions
      })
      .then(res => res.data.vote);
  },
  follow(_, followOptions) {
    return client
      .mutate({
        mutation: queries.follow,
        variables: followOptions
      })
      .then(res => res.data.follow);
  },
  addArgument(_, argumentOptions) {
    return client
      .mutate({
        mutation: queries.addArgument,
        variables: argumentOptions
      })
      .then(res => res.data.addArgument);
  },
  updateInfo(context, updateOptions) {
    return client
      .mutate({
        mutation: queries.updateInfo,
        variables: updateOptions
      })
      .then(res => res.data.updateInfo)
      .then(data => {
        const info = {
          country: data.country,
          about: data.about,
          name: data.name
        };

        context.commit("update_info", info);
      });
  },
  uploadImage(context, updateOptions) {
    return client
      .mutate({
        mutation: queries.uploadImage,
        variables: updateOptions
      })
      .then(res => res.data.uploadImage)
      .then(({ url }) => context.commit("add_image", { image: url }));
  },
  feed(_, feedOptions) {
    return client
      .query({
        query: queries.feed,
        variables: feedOptions
      })
      .then(res => res.data.feed);
  },
  removeRequest(context, request) {
    return context.commit("remove_request", request);
  },
  addDebate(context, debate) {
    context.commit("add_new_debate", debate);
    return debate;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
