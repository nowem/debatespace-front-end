import { client } from "@/plugins/apollo";
import query from "@/queries/auth";

const state = () => ({
  status: "",
  token: ""
});

const getters = {
  isLoggedIn: state => !!state.token,
  status: state => state.status,
  user: (_state, _getters, _rootState, rootGetters) =>
    rootGetters["account/user"]
};

const mutations = {
  auth_request(state) {
    state.status = "loading";
  },

  auth_success(state, { token }) {
    state.status = "success";
    state.token = token;
  },

  auth_error(state) {
    state.status = "error";
  },

  logout(state) {
    state.status = "";
    state.token = "";
  }
};

/** Unpack data object from query */
const unpack = query => ({ data }) => data[query];

/** Check the response for errors */
const verify = async ({ status, result, error }) => {
  if (status === "error") throw new Error(error);

  return result;
};

const auth = {
  /** Send authentication token for auto log in */

  recover: token => {
    return client
      .query({
        query: query.recover,
        variables: { token }
      })
      .then(unpack("authenticate"))
      .then(verify);
  },

  /** Send login request */
  login: ({ username, password }) => {
    return client
      .mutate({
        mutation: query.login,
        variables: {
          username,
          password
        }
      })
      .then(unpack("login"))
      .then(verify);
  },

  /** Register new user */
  register: ({ username, email, password, options }) => {
    return client
      .mutate({
        mutation: query.signup,
        variables: {
          username,
          email,
          password,
          options
        }
      })
      .then(unpack("signup"))
      .then(verify);
  }
};

/**
 * Creates a promise for auth response that will
 * commit appropriate mutation when resolved.
 *
 * @param {Auth Action} action
 * @param {Auth Arguments} args
 */
const handleAuthAction = (action, args, { commit, dispatch }) => {
  return new Promise(async (resolve, reject) => {
    commit("auth_request");

    try {
      const { token, user } = await action(args);
      commit("auth_success", { token });

      await dispatch("account/loadUser", user, { root: true });

      resolve({ token, user });
    } catch (err) {
      commit("auth_error");
      reject(err);
    }
  });
};

const actions = {
  recover: (context, token) => handleAuthAction(auth.recover, token, context),
  login: (context, credentials) =>
    handleAuthAction(auth.login, credentials, context),
  register: (context, user) => handleAuthAction(auth.register, user, context),
  reset: context => context.commit("logout"),

  logout({ commit, dispatch }) {
    return new Promise(async resolve => {
      commit("logout");
      await dispatch("account/logout", null, { root: true });
      resolve();
    });
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
