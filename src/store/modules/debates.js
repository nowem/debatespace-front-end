import { client as apollo } from "@/plugins/apollo";

import debates from "@/queries/debate";
import requests from "@/queries/request";

const actions = {
  /** Fetch 10 debates from all debates */
  async fetch(context, { page = 0, size = 10 }) {
    return apollo
      .query({
        query: debates.debates,
        variables: { page, size }
      })
      .then(res => res.data.debates);
  },

  createRequest(context, { initiator, recipient, options }) {
    return apollo
      .mutate({
        mutation: requests.createRequest,
        variables: { initiator, recipient, options }
      })
      .then(res => res.data);
  },
  resolveRequest(context, { request, status }) {
    context.dispatch("account/removeRequest", request, { root: true });
    return apollo
      .mutate({
        mutation: requests.resolveRequest,
        variables: { request, status }
      })
      .then(res => res.data)
      .then(data => data.resolveRequest)
      .then(debate =>
        context.dispatch("account/addDebate", debate, { root: true })
      );
  }
};

export default {
  namespaced: true,
  actions
};
