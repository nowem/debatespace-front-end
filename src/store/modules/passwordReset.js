import { client } from "@/plugins/apollo";
import queries from "@/queries/passwordReset";

const actions = {
  async verify(context, identifier) {
    const result = await client.query({
      query: queries.verify,
      variables: { identifier }
    });

    const data = result.data['verifyPasswordResetIdentifier'];
 
    if (data.status == 'error') {
      return data.error;
    }

    return null;
  },

  async reset(context, { identifier, password }) {
    const result = await client.mutate({
      mutation: queries.reset,
      variables: { identifier, password }
    });

    const data = result.data['resetPassword'];

    if (data.status == 'error') {
      return data.error;
    }

    return null;
  }
};

export default {
  namespaced: true,
  actions
};
