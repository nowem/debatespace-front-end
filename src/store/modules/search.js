import { client } from "@/plugins/apollo";
import search from "@/queries/search";

const actions = {
  users(context, filters) {
    return client
      .query({
        query: search.users,
        variables: filters
      })
      .then(res => res.data.searchUsers);
  },
  followers(context) {
    console.log("searching followers");
    return client
      .query({
        query: search.followers
      })
      .then(res => res.data.followers);
  }
};

export default {
  namespaced: true,
  actions
};
