module.exports = {
  css: {
    extract: { ignoreOrder: true },
    loaderOptions: {
      sass: {
        data: `@import "~@/css/main.scss"`
      }
    }
  },
  chainWebpack: config => {
    ["vue-modules", "vue", "normal-modules", "normal"].forEach(match => {
      config.module
        .rule("scss")
        .oneOf(match)
        .use("sass-loader")
        .tap(opt =>
          Object.assign(opt, { data: `@import '~@/css/main.scss';` })
        );
    });
  }
};
